<?php
/**
 * @file
 * Form builder class to alter the node form.
 */

namespace Drupal\node_published_date\Form;

/**
 * Class NodeFormAlter
 * @package Drupal\node_published_date\Form
 */
class NodeFormAlter {

  /**
   * Date format for the publication date on the form.
   */
  const DATE_FORMAT = 'Y-m-d H:i:s';

  /**
   * Build the form.
   *
   * @param array $form
   * @param array $form_state
   */
  public function buildForm(array &$form, array &$form_state) {
    $node = $this->getArgument($form_state);

    $published = NULL;
    if (!empty($node->published)) {
      $published = date(self::DATE_FORMAT, $node->published);
    }

    $form['options']['published'] = array(
      '#type' => module_exists('date_popup') ? 'date_popup' : 'textfield',
      '#title' => t('Published at date'),
      '#description' => t('The date that this node was published, in the format: %example', array(
        '%example' => date(self::DATE_FORMAT, REQUEST_TIME),
      )),
      '#default_value' => $published,
      '#date_format' => self::DATE_FORMAT,
      '#element_validate' => [
        [$this, 'checkIsValidTimeString'],
      ],
    );
  }

  /**
   * Ensure that a string entered into the published field is valid.
   *
   * @param array $element
   * @param array $form_state
   */
  public function checkIsValidTimeString($element, $form_state) {
    if ($element['#type'] !== 'textfield') {
      return;
    }

    $value = $element['#value'];
    $timestamp = strtotime($value);
    if (!$value || $timestamp !== FALSE) {
      return;
    }

    form_error($element, t('You must provide a valid date.'));
  }

  /**
   * Get an argument from a form state array.
   *
   * @param array $form_state
   * @param int $index
   *
   * @return mixed|null
   */
  protected function getArgument(array $form_state, $index = 0) {
    if (!isset($form_state['build_info']['args'][$index])) {
      return NULL;
    }
    return $form_state['build_info']['args'][$index];
  }

  /**
   * Attach the published date property to the node entity.
   *
   * @param \stdClass $node
   * @param array $form
   * @param array $form_state
   */
  public static function nodeSubmit(\stdClass $node, array $form, array &$form_state) {
    $published = strtotime($form_state['values']['published']);
    if ($published === FALSE) {
      if ((int) $node->status === NODE_PUBLISHED) {
        $published = REQUEST_TIME;
      }
      else {
        $published = NULL;
      }
    }

    $node->published = $published;
  }

}
